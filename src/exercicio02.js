const usuarios = [
 { nome: 'Diego', idade: 23, empresa: 'Rocketseat' },
 { nome: 'Gabriel', idade: 15, empresa: 'Rocketseat' },
 { nome: 'Lucas', idade: 30, empresa: 'Facebook' },
];
console.log(usuarios);	
//2.1
var result = usuarios.map((u)=>u.idade);
console.log(result);
//2.2
result = usuarios.filter(u=>(u.empresa == "Rocketseat" && u.idade>18));
console.log(result);
//2.3
result = usuarios.find(u=>(u.empresa == "google"));
console.log(result);
//2.4
result = usuarios.map((u)=>({nome:u.nome, idade:u.idade*2, empresa:u.empresa}));
result = result.filter(u => u.idade < 50);
console.log(result);