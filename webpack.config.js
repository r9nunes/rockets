var path = require('path');

module.exports = {
	mode: 'development',
	entry: ['@babel/polyfill','./src/main.js'],
	output: {
		path: path.join(__dirname + '/public'),
		filename: 'bundle.js'
	}, 
	devServer:{
		contentBase: path.join(__dirname + '/public'),
		host: '0.0.0.0',
		port: 80,
		headers: {
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
			"Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
  		}
	},
	module:{
		rules: [
			 {
			 	test: /\.js$/,
				exclude: /node_modules/,
			 	use: {
			 		loader: 'babel-loader'
			 	}
			 }
		],
	},
};