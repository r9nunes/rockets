const arr = [1, 2, 3, 4, 5, 6];
var [x, ...y] = arr;
console.log(`x: ${x} e y:${y}`);

function soma(a, b, ...c){	
	if (c.length > 1)
		c = c.reduce((t,s)=> t+s);
	else
		c = 0;
	return a + b + c;
}

console.log(soma(1, 2, 3, 4, 5, 6)); // 21
console.log(soma(1, 2)); // 3


function myFun(a, b, ...manyMoreArgs) {
  console.log("a", a); 
  console.log("b", b);
  console.log("manyMoreArgs", manyMoreArgs); 
}

myFun("one", "two", "three", "four", "five", "six");

// Console Output:
// a, one
// b, two
// manyMoreArgs, [three, four, five, six]


const usuario = {
 nome: 'Diego',
 idade: 23,
 endereco: {
 cidade: 'Rio do Sul',
 uf: 'SC',
 pais: 'Brasil',
 }
};

const usuario2 = {...usuario, nome:'Gabriel'};
console.log(usuario2);
const usuario3 = {...usuario, cidade:'lontras'};
console.log(usuario3);

