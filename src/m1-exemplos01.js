

/*
aula: arrays

const arr = [1,3,5,7,9];

const narr = arr.map(function(item, index){ 
	return item+index;
});

const sum = narr.reduce(function(total, next){ 
	return total+next;
});

console.log(narr);
console.log(sum);*/

/*
aula: const e let


function teste(x){
	let y = 2;
	if (x > 5){
		console.log(x,y);
	}
}

teste(10);*/

/* aula: classes
class TodoList1{
	constructor(){
		this.todos = [];
	}

	addTodo(){
		this.todos.push('Novo todo.');
		console.log(this.todos);
	}
}


class List{
	constructor(){
		this.data = [];
	}
	add(data){
		this.data.push(data);
		console.log(this.data);
	}
}
class TodoList2 extends List{
	constructor(){
		super();

		this.usuario = "R";
	}

	mostraUsuario(){
		console.log(this.usuario);
	}
}

class TodoList3{
	constructor(){
		this.todos=[];
	}

	static addTodo(){
		this.todos.push("novo todo ...");//ERRo!
		console.log(this.todos);
	}
}
var MinhaLista = new TodoList3();

TodoList3.addTodo();
TodoList3.addTodo();
TodoList3.addTodo();
TodoList3.addTodo();


var MinhaLista = new TodoList();
document.getElementById("novotodo").onclick = function(){
	MinhaLista.add('novo todo..');
};
MinhaLista.mostraUsuario();*/